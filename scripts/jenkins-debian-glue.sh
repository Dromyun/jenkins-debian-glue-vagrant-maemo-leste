#!/bin/bash

# install openjdk and jenkins
wget -q -O - http://pkg.jenkins-ci.org/debian-stable/jenkins-ci.org.key | apt-key add
echo "deb http://pkg.jenkins-ci.org/debian-stable binary/" > /etc/apt/sources.list.d/jenkins.list
apt-get update \
&& apt-get install -y openjdk-8-jdk \
&& apt-get install -y jenkins

# install jenkins-debian-glue
apt-get install -y jenkins-debian-glue \
&& rm -R /var/lib/jenkins/config.xml \
&& wget https://gitlab.com/Dromyun/jenkins-debian-glue-vagrant-maemo-leste/raw/master/apply.sh \
&& sudo bash ./apply.sh Temproot

# configure remote repository is optional
apt-get install -y apache2 \
&& sudo ln -s /srv/repository /var/www/html/debian \

# install pip3 and libs the python3
apt-get install -y python3-pip \
&& wget https://gitlab.com/Dromyun/jenkins-debian-glue-vagrant-maemo-leste/raw/master/requirement.txt \
&& pip3 install -r requirement.txt

#download jenkins-integration
git clone https://gitlab.com/Dromyun/jenkins-integration-i386-maemo-leste.git \
&& mv jenkins-integration-i386-maemo-leste /var/lib/jenkins/jenkins-integration
